# xmltool


xmltool is a collection of python scripts and templates for the automatic 
generation of code for reading and writing from XML files and storing the 
information in structured data types. 



## usage:

* insert your schema XML on the xsd directory
* edit the `generate_qes_libs_modules.py` script inserting the path to your 
  schema in  the `input_xsd` variable 
* run the script  

